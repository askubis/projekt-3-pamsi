#ifndef GRA_H
#define GRA_H

#include "ekran.h"
using namespace std;


class KolkoKrzyzyk {
public:
    // konstruktor klasy KolkoKrzyzyk
    KolkoKrzyzyk(const int _rozmiar_gry, const int _rozmiar_rzedu);

    // destruktor
    ~KolkoKrzyzyk();

    // glowna funkcja gry, ktora przeprowadza rozgrywke
    void rozgrywka();

private:
    int** plansza; // reprezentacja planszy, na ktorej toczy sie gra
    // 2 - x, 1 - o, 0 - puste
    const int rozmiar_gry;    // rozmiar planszy
    const int rozmiar_rzedu;  // rozmiar wygrywajacego rzedu
    pair<int, int> ruch; // aktualnie wykonywany ruch
    int tura;                 // informacja o graczu, ktory ma teraz ruch
    int zwyciezca = 0;        // informacja o zwyciezcy gry (0 to remis)
    int kolejka = 0;          // liczba wykonanych kolejek lacznie przez obu graczy
    pair<int, int> ostateczny_ruch;
    // optymalny ruch znaleziony przez algorytm
    Ekran ekran;       // obiekt reprezentujacy interfejs gry
    fstream file; // obiekt plik, do ktorego dokonywane jest zapis rozgrywki


    // sprawdzenie czy pole o podanych wspolrzednych jest w obrebie planszzy
    bool poprawneWspolrzedne(const int x, const int y);

    // sprawdz czy przez gracza zostal utworzony rzad znakow o okreslonej dlugosci
    // ktory konczy gre (sprawdza tylko od pola, na ktorym postawiono znak, zeby
    // nie przeszukiwac calej planszy)
    bool sprawdz_wygrana(const int gracz, const int px, const int py);

    // implementacja algorytmu minmax w celu gry z komputerem
    int minimax(const int poziom, const int gracz);
};


#endif
