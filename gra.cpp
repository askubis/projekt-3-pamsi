#include "gra.h"

using namespace std;


// sprawdzenie czy pole o podanych wspolrzednych jest w obrebie planszzy // je�li poprawne to zwraca 1, je�li nie to 0 
bool KolkoKrzyzyk::poprawneWspolrzedne(const int x, const int y) {
    return x >= 1 && y >= 1 && x <= rozmiar_gry && y <= rozmiar_gry;
}

// sprawdz czy przez gracza zostal utworzony rzad znakow o okreslonej dlugosci
// ktory konczy gre (sprawdza tylko od pola, na ktorym postawiono znak, zeby
// nie przeszukiwac calej planszy)
bool KolkoKrzyzyk::sprawdz_wygrana(const int gracz, const int px, const int py) { // py- cyfry, px - litery
    int x, y;

    // sprawdzenie kolumny zawierajacej pole x, y
    int rzad_znakow = 0;
    const int s = rozmiar_rzedu - 1;
    for (y = py + s, x = px; y >= py - s; y--) // sprawdzamy np jak ma by� 5 znak�w wygrywaj�cych to 4 do przodu i 4 do ty�u czy s� takie same - W KOLUMNIE
        if (poprawneWspolrzedne(x, y) && plansza[x][y] == gracz) { // je�li dane pole ma poprawne wsp�rz�dne i jest r�wne wybranemu graczowi to wykonuj poni�sz� p�tl�
            if (++rzad_znakow == rozmiar_rzedu) // je�li osi�gniemy podany rozmiar rz�du (ile znak�w wygrywa) to wtedy zwracamy true - WYGRANA 
                return true;
        }
        else
            rzad_znakow = 0; // zerujemy rzad_znakow 

    // sprawdzenie wiersza zawierajacego pole x, y // TO SAMO TYLKO DLA WIERSZY 
    rzad_znakow = 0;
    for (x = px - s, y = py; x <= px + s; x++) // sprawdzamy np jak ma by� 5 znak�w wygrywaj�cych to 4 do przodu i 4 do ty�u czy s� takie same, DLA WIERSZA 
        if (poprawneWspolrzedne(x, y) && plansza[x][y] == gracz) {
            if (++rzad_znakow == rozmiar_rzedu)
                return true;
        }
        else
            rzad_znakow = 0;

    // sprawdzenie lewej przekatnej zawierajacej pole x, y
    rzad_znakow = 0;
    for (y = py + s, x = px - s; y >= py - s; y--, x++)
        if (poprawneWspolrzedne(x, y) && plansza[x][y] == gracz) {
            if (++rzad_znakow == rozmiar_rzedu)
                return true;
        }
        else
            rzad_znakow = 0;

    // sprawdzenie prawej przekatnej zawierajacej pole x, y
    rzad_znakow = 0;
    for (y = py + s, x = px + s; y >= py - s; y--, x--)
        if (poprawneWspolrzedne(x, y) && plansza[x][y] == gracz) {
            if (++rzad_znakow == rozmiar_rzedu)
                return true;
        }
        else
            rzad_znakow = 0;

    return false;
}

// implementacja algorytmu minmax w celu gry z komputerem // GRAMY Z PERSPEKTYWY KRZY�YKA
int KolkoKrzyzyk::minimax(const int poziom, const int gracz) {

    const int POZIOMY_MINMAX[10] = { 3, 3, 3, 2, 2, 2, 2, 2, 1, 1 };
    int najlepsza = (gracz == 1 ? 20 : -10), wartosc; // zdeklarowanie zmiennej najlepsza i wartosc, gdy kolej kolka to najlepsza to 20, krzyzyka -10
    pair<int, int> najlepszy_ruch = { 0, 0 };

    // przejrzenie wszystkich mozliwych ruchow z obecnej konfiguracji
    for (int y = 1; y <= rozmiar_gry; y++)
        for (int x = 1; x <= rozmiar_gry; x++) {
            if (plansza[x][y])
                continue;

            plansza[x][y] = gracz; // wykonaj dany ruch, na pocz�tku to ruch komputera, gracz ==2, wtedy funkcja wykonuje si� pierwszy raz; 

            // sprawdz czy wykonany ruch jest optymalny
            if (gracz == 1) { // minimalizacja wyniku - gdy graczem jest u�ytkownik KӣKA MINIMALIZUJEMY WYNIK 

                if (sprawdz_wygrana(1, x, y)) // Sprawdzenie czy nie ma ju� wygranej k�ka 
                    wartosc = poziom; // aby poni�ej wykona� si� if
                else if (poziom < POZIOMY_MINMAX[rozmiar_gry - 1]) // dop�ki nie przekroczymy g��boko�ci
                    wartosc = minimax(poziom + 1, 2); // rekurencja, symulacja kolejnego ruchu, dla krzy�yka
                else
                    wartosc = 5; // w przeciwnym wypadku, gdy nie wygra�o k�ko, gdy "brakuje g��boko�ci", nie mo�emy wykona� dalszych symulacji ruchu
                                // warto�� = 5, �eby wykona� si� poni�szy if

                // zapisanie najlepszego ruchu 
                if (wartosc < najlepsza) {
                    najlepsza = wartosc;
                    najlepszy_ruch = { x, y };
                }

            }
            else { // maksymalizacja wyniku - gdy graczem jest komputer MAKSYMALIZUJAMY WYNIK 

                if (sprawdz_wygrana(2, x, y))  // Sprawdzenie czy nie ma ju� wygranej krzy�yka
                    wartosc = 10 - poziom; // aby wykona� si� poni�szy if 
                else if (poziom < POZIOMY_MINMAX[rozmiar_gry - 1]) // dop�ki nie przekroczy g��boko�ci 
                    wartosc = minimax(poziom + 1, 1); // rekurencja, symulacja kolejnego ruchu, dla  k�ka
                else
                    wartosc = 5; // w przeciwnym wypadku, gdy nie wygra� krzy�yk, gdy "brakuje g��boko�ci", nie mo�emy wykona� dalszych symulacji ruchu
                                // warto�� = 5, �eby wykona� si� poni�szy if
                // zapisanie najlepszego ruchu 
                if (wartosc > najlepsza) { // najlepsza = -10
                    najlepsza = wartosc;
                    najlepszy_ruch = { x, y };
                }
            }
            plansza[x][y] = 0; // wycofaj tymczasowych ruch i sprawdz kolejny
        }

    // remis jesli nie ma zadnego ruchu prowadzacego do wygranej
    if (ostateczny_ruch.first == 0)
        najlepsza = 5;
    ostateczny_ruch = najlepszy_ruch;

    // zwroc najlepsza wartosc ruchu
    return najlepsza;
}

// konstruktor klasy KolkoKrzyzyk
KolkoKrzyzyk::KolkoKrzyzyk(const int _rozmiar_gry, const int _rozmiar_rzedu)
    : rozmiar_gry(_rozmiar_gry), rozmiar_rzedu(_rozmiar_rzedu), ekran(_rozmiar_gry) {

    // alokacja pamieci dla planszy o podanych wymiarach
    plansza = new int* [rozmiar_gry + 1];
    for (int i = 0; i <= rozmiar_gry; i++) {
        plansza[i] = new int[rozmiar_gry + 1]{};
    }
}

// destruktor
KolkoKrzyzyk::~KolkoKrzyzyk() {
    for (int i = 0; i < rozmiar_gry; i++)
        delete[] plansza[i];
    delete[] plansza;
}

// glowna funkcja gry, ktora przeprowadza rozgrywke
void KolkoKrzyzyk::rozgrywka() {
    
    // otwierany jest plik, do ktorego ma zostac dokonany zapis rozgrywki
    file.open("zapis", ios::out);
    if (!file.good()) {
        cout << "\n\tBlad podczas otwierania pliku.\n";
        return;
    }
    // do pliku zapisywana jest informacja o rozmiarze planszy
    file << "rozmiar " << rozmiar_gry << endl;

    tura = rand() % 2 + 1; // losowy gracz zaczyna, wylosowanie kto ma ruch, losuje albo 1 albo 2

    while (!zwyciezca) { // dop�ki zwyciezca = 0

        ekran.wyswietlGre(plansza); // wyswietl plansze, ktora tworzy sie w konstruktorze, na pocz�tku ma same puste pola 

        if (tura == 1) { // je�li kolej k�ka 
            ruch = ekran.wczytajRuch(tura); // wczytaj ruch od uzytkownika
            // ruch niepoprawny jesli pole jest juz zajete
            if (ruch.first && plansza[ruch.first][ruch.second])
                ruch = { 0, 0 };
        }
        else { // w przeciwnym wypadku, gdy tura nie jest r�wna 1, czyli nie jest to ruch gracza to ruch wykonuje komputer 
            minimax(0, tura);
            ruch = ostateczny_ruch;
        }

        // jesli ruch jest niepoprawny, to wyswietl blad i wczytaj ruch ponownie
        if (ruch.first == 0) {
            ekran.wyswietlBlad();
            continue; // wejscie od pocz�tku do while 
        }

        plansza[ruch.first][ruch.second] = tura; // dodaj nowy ruch, przypisuje do planszy ruch gracza 
        if (sprawdz_wygrana(tura, ruch.first, ruch.second))// sprawdz czy gra zostala zakonczona/
            zwyciezca = tura; // je�li gra zako�czona to przypisz zwyci�zce 
        // zapisz wykonany ruch do pliku
        file << (tura == 1 ? "o: " : "x: ") << (char)(ruch.first + 'a' - 1);
        file << ruch.second << endl;

        tura = tura % 2 + 1; // ruch drugiego gracza

        if (++kolejka == rozmiar_gry * rozmiar_gry) // je�li kolejka jest wi�ksza od liczby p�l to wychodzi z while 
            break;
    }

    // do pliku zapisywana jest informacja o zwyciezcy
    switch (zwyciezca) {
    case 0:
        file << "remis" << endl;
        break;
    case 1:
        file << "wygral o" << endl;
        break;
    case 2:
        file << "wygral x" << endl;
        break;
    }
    file.close(); // zamykanie pliku po zakoczeniu zapisu

    ekran.wyswietlGre(plansza);

    ekran.wyswietlKoniec(zwyciezca);
}