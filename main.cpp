﻿#include "gra.h"
#include "ekran.h"
#undef max

using namespace std;


// pobiera od uzytkownika informacje o wyborze w menu gry
// zwraca 1, 2, 3 - zaleznie od wyboru lub 0 jesli byl on niepoprawny i konieczne
// jest powtorzenie
int wczytajLiczbe() {
    int liczba;
    // wczytaj opcje od uzytkownika
    cout << "\n  ";
    if (!(cin >> liczba))
        liczba = 0;
    // zabezpieczenie na wypadek gdyby uzytkownik wpisal np. litery
    // bez tego program moglby sie zawiesic
    // wszystko co nie jest poprawnym wyborem jest ignorowane
    cin.clear();
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    return liczba;
}

int main() {

    int rozmiar_gry, rozmiar_rzedu;

    bool poprawne = false;
    while (!poprawne) { // dopóki poprawne to false 
        Ekran::wyczysc();
        cout << "\n  Podaj rozmiar planszy 3 - 10" << endl;
        rozmiar_gry = wczytajLiczbe(); // wczytanie liczby (rozmiaru gry) z klawiatury, przy pomocy funkcji wczytajLiczbe() 
        if (rozmiar_gry >= 3 && rozmiar_gry <= 10)
            poprawne = true; // poprawne ustawia na true gdy wpiszemy liczby z zakresu <3,10>, czyli wychodzi z while, jeśli nie to znowu prosi o wybranie liczby 
    }

    poprawne = false;
    while (!poprawne) {  // dopóki poprawne to false 
        Ekran::wyczysc();
        cout << "\n  Wybierz liczbe elementow w wygrywajacym rzedzie 3 - ";
        cout << rozmiar_gry << endl; // liczba elementów wygrywających musi być minimalnie 3 i max rozmiar_gry 
        rozmiar_rzedu = wczytajLiczbe(); // wczytanie liczby (rozmiaru rzędu wygrywającego) z klawiatury, przy pomocy funkcji wczytajLiczbe() 
        if (rozmiar_rzedu >= 3 && rozmiar_rzedu <= rozmiar_gry)  
            poprawne = true; // poprawne ustawia na true gdy wpiszemy liczby z zakresu <3,rozmiar_gry>, czyli wychodzi z while, jeśli nie to znowu prosi o wybranie liczby
    }

    KolkoKrzyzyk gra(rozmiar_gry, rozmiar_rzedu); // utworz gre z podanymi parametrami / utworzenie obiektu gra
    gra.rozgrywka();                              // rozpocznij gre, funkcja rozgrywka

    return 0;
}