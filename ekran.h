#pragma once
#ifndef EKRAN_H
#define EKRAN_H

#include <cstdlib>
#include <ctime>
#include <fstream>
#include <iostream>
#include <limits>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <utility>


#include <conio.h>
#include <windows.h>

using namespace std;


class Ekran {
public:
    // konstruktor klasy Ekran
    Ekran(const int _rozmiar_gry);

    // czysci ekran, aby umozliwic wyswietlanie w tym samym miejscu
    static void wyczysc();

    // wyswietla aktualny stan gry, pokazujac wszystkie ustawione znaki
    void wyswietlGre(int** plansza);

    // wczytuje ruch od uzytkownika, sprawdzajac jego poprawnosc
    pair<int, int> wczytajRuch(int tura);

    // pokaz komunikat, ze ruch zostal blednie wpisany
    void wyswietlBlad();

    // na zakonczenie gry pokaz komunikat o zwyciezcy lub remisie
    void wyswietlKoniec(int gracz);

private:
    const int rozmiar_gry; // bok kwadratu, na ktorym toczy sie gra

    // pazuje program na podana liczbe sekund
    void pauza(int czas);

    // odczytuje numer kolumny na podstawie jej oznaczenia literowego
    int naKolumne(char x);
};

#endif
