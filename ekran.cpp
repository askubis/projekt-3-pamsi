#include "ekran.h"
#undef max
using namespace std;


// czysci ekran, aby umozliwic wyswietlanie w tym samym miejscu
void Ekran::wyczysc() {

    system("cls");

}

// pazuje program na podana liczbe sekund
void Ekran::pauza(int czas) {

    Sleep(czas * 1000);
}

// wyswietla aktualny stan gry, pokazujac wszystkie ustawione znaki
void Ekran::wyswietlGre(int** plansza) {
    wyczysc();

    // pokazanie liter na gorze (oznaczenia kolumn)
    cout << "\n    ";
    for (int x = 0; x < rozmiar_gry; x++)
        cout << static_cast<char>('a' + x) << " "; // wypisanie tyle liter ile wynosi rozmiar gry / rzutowanie 
    cout << endl;

    // wyswietl plansze wiersz po wierszu
    for (int y = rozmiar_gry; y; y--) {

        // NAJPIERW WY�WIETLENIE NUMERU WIERSZA 
        // pokazanie numeru wiersza, numer�w w pionie (numer 10 wyswietlany o jedna spacje wczesniej)
        if (y < 10)
            cout << "  " << y;
        else
            cout << " 10"; // 10 wy�wietlam o jedn� spacj� wcze�niej, �eby nie zaburzy� wygl�du planszy 

        
        // pokaz stan danego wiersza planszy
        for (int x = 1; x <= rozmiar_gry; x++) {
            cout << " "; // SPACJA

            if (!plansza[x][y]) // JE�LI dane POLE jest PUSTE to wy�wietl PUSTE MIEJSCE 
                cout << " "; 
            else if (plansza[x][y] == 1) { //ALBO  WYPISANIE KӣKA, gdy dany element planszy to 1 
                // kolko
                cout << "O";
            }
            else if (plansza[x][y] == 2) { // ALBO WYPISANIE KRZY�YKA gdy element planszy to 2
                // krzyzyk
                cout << "X";
            }
        }
        cout << endl;
    }
}

// odczytuje numer kolumny na podstawie jej oznaczenia literowego // char to "a" - zwraca 1, b - 2, c - 3 
int Ekran::naKolumne(char x) {
    if (x >= 'a' && x <= 'z')
        return x - 'a' + 1;
    else if (x >= '0' && x <= '9')
        return x - '1' + 1;
    else
        return 0;
}

// konstruktor klasy Ekran
Ekran::Ekran(const int _rozmiar_gry) : rozmiar_gry(_rozmiar_gry) {}

// wczytuje ruch od uzytkownika, sprawdzajac jego poprawnosc
pair<int, int> Ekran::wczytajRuch(int tura) { // pair bo zwracany jest wynik, kt�ry jest typu pair 

    cout << "\n  Gracz " << (tura == 1 ? 'O' : 'X') << ":  "; // je�li tura = 1, "Gracz O" Je�li tura = 2 "Gracz X" 
    pair<int, int> wynik; // wynik.first - litera, wynik.second - cyfra 
    string wczytaj;
    cin >> wczytaj; // wczytaj ciag znakow z klawiatury
    cin.clear(); // ZABEZPIECZENIE 
    cin.ignore(numeric_limits<streamsize>::max(), '\n');

    // przetwarzanie ciagu znakow wpisanego przez uzytkownika
    int r = 0;
    while (wczytaj[r] == '\t' || wczytaj[r] == ' ') // pomijanie tabulacji i spacji w wczyttywaniu 
        ++r;

    bool poprawne = true;


    // wczytaj litere, ktora jest oznaczeniem kolumny
    if (isalpha(wczytaj[r]) && naKolumne(wczytaj[r]) >= 1 && // isalpha - Sprawdza czy znak przekazany jako argument jest liter� alfabetu.
                                                             // zwraca warto�� r�n� od zera gdy argument, kt�ry zosta� przekazany do funkcji jest liter� alfabetu. 
                                                             // W przeciwnym wypadku funkcja zwraca warto�� zero.
                                                             // 
                                                             // czy wczytany znak jest liter� alfabetu && czy ta litera jest w odpowiednim przedziale od 1 do rozmiarugry 
        naKolumne(wczytaj[r]) <= rozmiar_gry)
        wynik.first = naKolumne(wczytaj[r++]); // wczytuje numer odpowiadaj�cy wpisanej literze, dla a -> 1 b-> 2 itd. I potem wykonuje ikrementacj�.  
    else
        poprawne = false; // gdy �le litera nie jest z przedzia�u od a do rozmiaru gry i nie jest liter� to poprawne = false; 

    // wczytaj liczbe, ktora jest oznaczeniem wiersza
    if (isdigit(wczytaj[r]) && naKolumne(wczytaj[r]) >= 1 && // isdigit zwraca warto�� r�n� od zera gdy argument, kt�ry zosta� przekazany do funkcji jest cyfr�. 
                                                             // W przeciwnym wypadku funkcja zwraca warto�� zero.
                                                            // czy wczytany znak jest cyfr� && czy ta cyfra jest w odpowiednim przedziale od 1 do rozmiarugry
        naKolumne(wczytaj[r]) <= rozmiar_gry) {
        if (rozmiar_gry == 10 && wczytaj[r] == '1' && wczytaj[r + 1] == '0') { // gdy wybrany wiersz to 10 
            wynik.second = 10; 
            r += 2;
        }
        else
            wynik.second = naKolumne(wczytaj[r++]); // wczytanie cyfry (wybranego wiersza do wynik.second i zrobienie inkrementacji 
    }
    else
        poprawne = false;

    if (!poprawne) // je�li �le wpisali�my pole to wynik = 0,0 
        wynik = { 0, 0 };

    return wynik;
}

// pokaz komunikat, ze ruch zostal blednie wpisany
void Ekran::wyswietlBlad() {
    cout << "\n  bledny ruch\n";
    pauza(1);
}

// na zakonczenie gry pokaz komunikat o zwyciezcy lub remisie
void Ekran::wyswietlKoniec(int gracz) {

    pauza(1);
    wyczysc();
    cout << "\n  ";
    if (gracz == 1) {
        cout << "Wygrana O\n";
    }
    else if (gracz == 2) {
        cout << "Wygrana X\n";
    }
    else if (gracz == 0)
        cout << "Remis\n";

    pauza(2); // zatrzymanie programu na 2s
}
